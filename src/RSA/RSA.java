package RSA;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Rsa classs who  regroup all
 * variables and function to crypt and decrypt RSA
 * @author Thibaut
 *
 */
public class RSA {
	
	
	//D�finition des variables de classes n�c�ssaires au RSAS 
	private BigInteger p,q,n,d,phi;
	
	// Choix de e
	private BigInteger e = BigInteger.valueOf(65537);
	
	
	/** The size. */
	private BigInteger size = new BigInteger("9999");
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RSA \n p = " + p + "\n q = " + q + "\n n=" + n + "\n d=" + d + "\n e=" + e+ "\n phi=" + phi+ "\n d=" + d + "\n size=" + size ;
	}


	/**
	 * Instantiates a new rsa.
	 */
	public RSA() {
		getPrimal();
	}
	
	public RSA(BigInteger size) {
		this.size = size;
		getPrimal();
	}
	
	
	public RSA(BigInteger p, BigInteger q, BigInteger e) {
		super();
		this.p = p;
		this.q = q;
		this.e = e;
		rsaCompute();
	}
	
	
	private void getPrimal() {
		PrimalNumber prime= new PrimalNumber();
		// Initialisation de q
		prime.nextRandomBigInteger(size);
		prime.Primal();
		q = prime.curr_number;
		// Initialisation de p
		prime.nextRandomBigInteger(size);
		prime.Primal();
		p = prime.curr_number;
	}
	
	public void rsaCompute() {
		// n = p*q
				n = p.multiply(q);
				
				// indicatrice d'euler 
				//phi(n)=(p-1)(q-1)
				phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
				
				//cl� priv�
				d  = e.modInverse(phi);
	}


	/**
	 * Encrypt message
	 *
	 * @param message the message
	 * @return crypted string
	 */
	public List<String> Encrypt(String message) {
		List<String> tmp = Utilities.toAscii(message);
		tmp = Utilities.toBloc(tmp,4);
		System.out.println(tmp);
		for(int i = 0;i<tmp.size();i++) {
			tmp.set(i, new BigInteger(tmp.get(i)).modPow(e, n).toString());
		}
		return tmp;
	}
	
	/**
	 * Decrypt message
	 * @param message
	 * @return
	 */
	public List<String> Decrypt(List<String> messageCrypted) {
		List<String> result = new ArrayList<String>();
		System.out.println(messageCrypted+"lis");

		for(String str : messageCrypted) {
			result.add(new BigInteger(str).modPow(d, n).toString());
		}
		System.out.println(result+"lis0");
		result = Utilities.addZero(result);
		System.out.println(result+"lis1");
		result = Utilities.toBloc(result,3);
		return result;
	}
	
	
	public BigInteger getP() {
		return p;
	}


	public void setP(BigInteger p) {
		this.p = p;
	}


	public BigInteger getQ() {
		return q;
	}


	public void setQ(BigInteger q) {
		this.q = q;
	}


	public BigInteger getN() {
		return n;
	}


	public void setN(BigInteger n) {
		this.n = n;
	}


	public BigInteger getD() {
		return d;
	}


	public void setD(BigInteger d) {
		this.d = d;
	}


	public BigInteger getPhi() {
		return phi;
	}


	public void setPhi(BigInteger phi) {
		this.phi = phi;
	}


	public BigInteger getE() {
		return e;
	}


	public void setE(BigInteger e) {
		this.e = e;
	}


	public BigInteger getSize() {
		return size;
	}


	public void setSize(BigInteger size) {
		this.size = size;
	}


}
