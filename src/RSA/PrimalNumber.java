package RSA;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Random;

public class PrimalNumber {
	public BigInteger curr_number;
	private int size = 15;
	
	public void nextRandomBigInteger(BigInteger n) {
		//Rand permet de g�n�rer des nombres al�atoires
	    Random rand = new Random();
	    
	    curr_number = new BigInteger(n.bitLength(), rand);
	    while( curr_number.compareTo(n) >= 0 ) {
	    	curr_number = new BigInteger(n.bitLength(), rand);
	    }
	    // Si number pair on ajoute 1
	    if(curr_number.mod(new BigInteger("2")).equals(BigInteger.ZERO))
	    	curr_number= curr_number.add(BigInteger.ONE);
	}
	
	public void Primal() {
		while (!curr_number.isProbablePrime(1)) {
			curr_number=curr_number.add(BigInteger.valueOf(2L));
		}
	}
}
