package RSA;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Utilities { 

	// Function to find modular inverse of a  
	// under modulo m Assumption: m is prime 
	static void modInverse(BigInteger a, BigInteger m) 
	{ 
		BigInteger g = a.gcd(m); 
		if (g != BigInteger.ONE) 
			System.out.println("Inverse doesn't exist"); 
		else 
		{ 
			// If a and m are relatively prime, then modulo inverse 
			// is a^(m-2) mode m 
			System.out.println("Modular multiplicative inverse is " + 
					power(a, m.subtract(new BigInteger("2")), m)); 
		} 
	} 

	// To compute x^y under modulo m 
	/**
	 * @param x
	 * @param y
	 * @param m
	 * @return
	 */
	static BigInteger power(BigInteger x, BigInteger y, BigInteger m)  
	{ 
		if (y == BigInteger.ZERO) 
			return BigInteger.ONE; 
		BigInteger p = power(x, y.divide(new BigInteger("2")),m).mod(m); 
		p = (p.multiply(p)).mod(m); 

		if (y.divide(new BigInteger("2")) == BigInteger.ZERO) 
			return p; 
		else
			return x.multiply(p).mod(m); 
	} 


	/**
	 * @param Input BigInteger
	 * @return  Biginteger to hexa string
	 */
	static String toHex(BigInteger Input) {
		return Input.toString(16);
	}


	/**
	 * @param Input a sstring 
	 * @return byte[] who contains ascii char
	 * @throws UnsupportedEncodingException if the function cant cast string in byte array
	 */
	static List<String> toAscii(String Input) {
		List<String> list = new ArrayList<String>();
		for(int i =0 ;i<Input.length();i++) {
			String tmp = Integer.toString((int)Input.charAt(i));
			if(tmp.length()>=3) 
			{
				list.add(tmp);
			}

			else 
			{
				list.add("0"+tmp);
			}
		}
		return list;
	}

	/**
	 * @param list
	 * @return list parsed by bloc of n char
	 */
	static List<String> toBloc(List<String> list,int n) {
		String tmp ="";
		List<String> newList = new ArrayList<String>();
		for(String str : list) {
			for(int i = 0;i<str.length();i++) {
				tmp = tmp + str.charAt(i);
				if(tmp.length() == n ) {
					newList.add(tmp);
					tmp = "";
				}
			}
		}
		if(!tmp.isEmpty()){
			newList.add(tmp);
		}
		System.out.println(newList);
		return newList;
	}
	
	public static List<String> addZero(List<String> list){
		List<String> newList = new ArrayList<String>();
		for(int i =0;i<list.size();i++) {
			String str = list.get(i);
			while(str.length()<4 && i<list.size()-1) {
				str = 0+str;
			}
			newList.add(str);
		}
		return newList;
	}

	public static String ListToString(List<String> list) {
		String result = "";
		for(String str : list) {
			result +=str+" ";
		}
		return result.trim();
	}

	public static List<String> StringToList(String str){
		str = str.trim();
		return Arrays.asList(str.split(" "));
	}

	public static String StringListToAscii(List<String> list){
		String result = "";
		for(String str : list) {
			result+= Character.toString ((char) Integer.parseInt(str));
		}
		return result.trim();
	}
} 

