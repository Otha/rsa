package UI;

import java.math.BigInteger;
import java.util.List;


import RSA.RSA;
import RSA.Utilities;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class Controller {
	private Boolean chiffrement = true;
	@FXML 
	private Button makeRSA_button,cypher_button; 
	@FXML
	private Label status,labmess;
	@FXML
	private TextField size_field,q_field,p_field,e_field;
	@FXML
	private TextArea message_ta,reponse_ta;
	@FXML
	private Text n_text,d_text,phi_text;
	@FXML
	private MenuButton cypher_ddl;
	@FXML
	private MenuItem chiffr_bu,dechiffr_bu;

	private RSA rsa;
	private List<String> result;
	@FXML  
	public void makeRSA() {  
		rsa = new RSA();
		status.setText("");
		if(!size_field.getText().isEmpty()) {
			rsa = new RSA(new BigInteger(size_field.getText()));
		}
		if(!e_field.getText().isEmpty()) {
			try {
				rsa.setE(new BigInteger(e_field.getText()));
			}catch(Exception e) {
				status.setText("La valeur de E doit  �tre num�rique");
				return;
			}
		}
		if(!p_field.getText().isEmpty()) {
			BigInteger tmp = new BigInteger("0");
			try {
				tmp = new BigInteger(p_field.getText());
			}catch(Exception e) {
				status.setText("La valeur de P doit  �tre num�rique");
				return;
			}
			if(tmp.isProbablePrime(2)) {
				rsa.setP(tmp);
			}else {
				status.setText("P doit  �tre un nombre premier");
			}
		}

		if(!q_field.getText().isEmpty()) {
			BigInteger tmp = new BigInteger("0");
			try {
				tmp = new BigInteger(q_field.getText());
			}catch(Exception e) {
				status.setText("La valeur de Q doit  �tre num�rique");
				return;
			}
			if(tmp.isProbablePrime(2)) {
				rsa.setQ(tmp);
			}else {
				status.setText("Q doit  �tre un nombre premier");
			}
		}
		rsa.rsaCompute();
		message_ta.setEditable(true);
		cypher_button.setVisible(true);
		e_field.setText(rsa.getE().toString());
		p_field.setText(rsa.getP().toString());
		q_field.setText(rsa.getQ().toString());
		n_text.setText(rsa.getN().toString());
		d_text.setText(rsa.getD().toString());
		phi_text.setText(rsa.getPhi().toString());
	}

	@FXML
	public void setChiffr() {
		chiffrement = true;
		cypher_ddl.setText("Chiffrement");
		labmess.setText("Message chiffr� : ");
		reponse_ta.setText("");
	}
	@FXML
	public void setDchiffr() {
		if(!reponse_ta.getText().isEmpty()) {
			message_ta.setText(reponse_ta.getText());
			reponse_ta.setText("");
		}
		labmess.setText("Message d�chiffr� : ");
		chiffrement = false;
		cypher_ddl.setText("D�chiffrement");
	}
	@FXML 
	public void chiffrement() {
		if(chiffrement) {
			List<String> result = rsa.Encrypt(message_ta.getText());
			reponse_ta.setText(Utilities.ListToString(result));
		}else {
			List<String> result = rsa.Decrypt(Utilities.StringToList(message_ta.getText()));
			reponse_ta.setText(Utilities.StringListToAscii(result));
		}
	}
}
